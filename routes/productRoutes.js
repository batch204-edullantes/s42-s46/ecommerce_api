const express = require("express");
const auth = require("../auth");
const productController = require("../controllers/productController");

const router = express.Router();

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.create(req.body, userData).then(resultFromController => res.send(resultFromController));
});

router.get("/", auth.verify, (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

router.get("/all", auth.verify, (req, res) => {
	productController.getAll().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", auth.verify, (req, res) => {
	productController.retrieve(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	productController.update(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/archive", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	productController.archive(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;