const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");

const router = express.Router();

router.post("/register", (req, res) => {
	userController.register(req.body).then(resultFromController => res.send(resultFromController));	
});

router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/checkout', auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.createOrder(req.body, userData).then(resultFromController => res.send(resultFromController));	
});	

router.get('/:email/userDetails', auth.verify, (req, res) => {
	userController.details(req.body, req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/cart", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.addToCart(req.body, userData).then(resultFromController => res.send(resultFromController));
});

router.get("/cart/products", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.cartProducts(userData).then(resultFromController => res.send(resultFromController));
});

router.delete("/cart/:productName", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.removeCartProduct(req.params, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;