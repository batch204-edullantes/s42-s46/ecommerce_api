const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const cors = require("cors");

const app = express();

const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://batch204:admin123@batch204.gsk1alv.mongodb.net/s42-s46?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", () => console.error.bind(console, "Error"));

db.once("open", () => console.log("Now connected at MongoDB Atlas!"));

app.use(cors());
app.use(express.json());

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});