const Product = require("../models/Product");
const auth = require("../auth");

module.exports.create = (reqBody, data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		
		return newProduct.save().then((saved, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	} else {
		return false;
	}
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {

		if(result == null) {
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getAll = () => {
	return Product.find().then(result => {
		console.log(result);
		if(result == null) {
			return false;
		} else {
			return result;
		}
	})
}

module.exports.retrieve = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if(result == null) {
			return false;
		} else {
			return result;
		}
	});
}

module.exports.update = (reqParams, reqBody, data) => {
	if(data.isAdmin) {
		return Product.findByIdAndUpdate(reqParams.productId, {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}).then((success, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	} else {
		return false;
	}
}

module.exports.archive = (reqParams, reqBody, data) => {
	if(data.isAdmin) {
		return Product.findByIdAndUpdate(reqParams.productId, {
			isActive: reqBody.isActive
		}).then((success, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	} else {
		return false;
	}
}