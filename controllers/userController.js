const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.register = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((success, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	});
}

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {acces: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	});
}

module.exports.createOrder = (reqBody, data) => {
	if(!data.isAdmin) {
		return User.findById(data.id).then(result => {
			result.orders.push(reqBody);

			result.save().then((success, error) => {``
				if(error) {
					return false;
				} else {
					return true;
				}
			}); 
		});
	} else {
		return false;
	}
}

module.exports.addToCart = (reqBody, data) => {
	if(!data.isAdmin) {
		return User.findById(data.id).then(result => {
			let is_exist = false;
			result.cart.products.map((value) => {
				if(value.productName == reqBody.productName) {
					value.quantity += reqBody.quantity;
					is_exist = true;
				}
			});

			if(!is_exist) {
				result.cart.products.push(reqBody);
			}
			
			result.save().then((success, error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			}); 
		});
	} else {
		return false;
	}
}

module.exports.cartProducts = (data) => {
	return User.findOne({email: data.email}).then(result => {
		return result.cart.products;
	})
}

module.exports.removeCartProduct = (reqParams, data) => {
	return User.findOne({email: data.email}).then(result => {
		let cart = [];
		result.cart.products.map((value) => {
			if(value.productName != reqParams.productName) {
				console.log(value.productName);
				console.log(reqParams.productName);
				cart.push(value);
			}
		});

		result.cart.products = cart;

		return result.save().then((success, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	})
}

module.exports.details = (reqBody, reqParams) => {
	return User.findOne({email: reqParams.email}).then(result => {
		console.log(result);
		if(result != null) {
			result.password = '';
			return result;
		} else {
			return false;
		}
	})
}